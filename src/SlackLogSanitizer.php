<?php

namespace Sirs\SlackLogSanitizer;

use Monolog\Handler\SlackWebhookHandler;

class SlackLogSanitizer
{
    public function __invoke($logger)
    {
        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof SlackWebhookHandler) {
                $handler->pushProcessor(function ($record) {
                    $extra = [
                        'file:line' => $record['context']['exception']->getFile().':'.$record['context']['exception']->getLine(),
                    ];
                    $record['context'] = $extra;
                    $record['message'] = substr($record['message'], 0, 15);
                    return $record;
                });
            }
        }
    }
}
